//
//  еntertainmentPlace.m
//  Мій Житомир
//
//  Created by Nazar Gorobets on 23.12.14.
//  Copyright (c) 2014 Nazar Gorobets. All rights reserved.
//

#import <MapKit/MapKit.h>
#import "еntertainmentPlace.h"

@implementation entertainmentPlace
@synthesize coordinate;
@synthesize title;
@synthesize subtitle;

-(MKAnnotationView *)annotationViewEntertainment{
    
    MKAnnotationView *annotationViewEntertainment = [[MKAnnotationView alloc] initWithAnnotation:self reuseIdentifier:@"entertainmentPlace"];
    
    annotationViewEntertainment.enabled=YES;
    annotationViewEntertainment.canShowCallout = YES;
    annotationViewEntertainment.image = [UIImage imageNamed:@"pinsEntertaiment"];
    annotationViewEntertainment.leftCalloutAccessoryView = [UIButton buttonWithType:UIButtonTypeInfoLight];
    annotationViewEntertainment.canShowCallout = YES;
    
    UIImageView *sfIconView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"man204"]];
    annotationViewEntertainment.rightCalloutAccessoryView = sfIconView;
    return annotationViewEntertainment;
}

@end
