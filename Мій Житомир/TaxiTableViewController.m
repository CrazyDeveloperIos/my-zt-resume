//
//  TaxiTableViewController.m
//  Мій Житомир
//
//  Created by Nazar Gorobets on 2/17/15.
//  Copyright (c) 2015 Nazar Gorobets. All rights reserved.
//

#import "TaxiTableViewController.h"
#import "TaxiNamber.h"
#import "MZWorkWithDB.h"
@interface TaxiTableViewController ()
@property (nonatomic, strong) NSMutableArray *taxiArray;
@end

@implementation TaxiTableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.taxiArray = [NSMutableArray new];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:NO];
    [self.navigationController setToolbarHidden:YES];
    self.taxiArray = [[[MZWorkWithDB date] getAllTaxi] mutableCopy];
    
}



- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {

    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {

    // Return the number of rows in the section.
    return [self.taxiArray count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell * cell = [tableView cellForRowAtIndexPath:indexPath];
    
    if (!cell){
        cell = [tableView dequeueReusableCellWithIdentifier:@"cellTaxiIndefier" forIndexPath:indexPath];
    }
    TaxiNamber *taxi = [self.taxiArray objectAtIndex:indexPath.row];
    cell.textLabel.text = taxi.nameTaxi;
    cell.detailTextLabel.text = [NSString stringWithFormat:@"%d", [taxi.phoneNamber intValue]];
    // Configure the cell...
    
    return cell;
}
-(void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath{
    if(UITableViewCellEditingStyleDelete){
        TaxiNamber *taxi = [self.taxiArray objectAtIndex:indexPath.row];
        [[MZWorkWithDB date] removeTaxiWithObject:taxi];
        [self.taxiArray removeObjectAtIndex:indexPath.row];
    }
    [self.tableView reloadData];
}

/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    } else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath {
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/


#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    
    
    
}

@end
