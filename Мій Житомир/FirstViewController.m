//
//  FirstViewController.m
//  Мій Житомир
//
//  Created by Yosemite c) 2014 Nazar Gorobets. All rights reserved.
//

#import "FirstViewController.h"
#import "FoodPlase.h"
#import "еntertainmentPlace.h"
#import <CoreLocation/CoreLocation.h>
#import <Social/Social.h>

@interface FirstViewController ()<CLLocationManagerDelegate>
@property (strong, nonatomic) CLLocationManager *locationManager;
@property (nonatomic) CLLocationCoordinate2D Coordinate;
@property (strong, nonatomic) UITableView *infoTableView;
@property (strong, nonatomic) MKDirections* directions;
@end


@implementation FirstViewController
@synthesize myTimerSetter;
@synthesize myTimerSetterTwo;

MKRoute *routeDetails;
MKDirectionsRequest* request;

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.map.delegate = self;
    [self.map setShowsUserLocation:YES];
    [self.map setUserTrackingMode:MKUserTrackingModeFollow animated:YES];
    self.locationManager = [[CLLocationManager alloc] init];
    self.locationManager.delegate = self;
    if ([self.locationManager respondsToSelector:@selector(requestWhenInUseAuthorization)]) {
        [self.locationManager requestWhenInUseAuthorization];
    }
    [self.locationManager startUpdatingLocation];
    self.infoTableView = [[UITableView alloc] initWithFrame:CGRectMake(0.0f, 0.0f, 200.0f, 300.0f)
                                                      style:UITableViewStylePlain];
    
#pragma mark - Челентано
    FoodPlase *annotation1 = [FoodPlase new];
    annotation1.title = @"Челентано";
    annotation1.subtitle = @"(097)33-22-34 09:00-21:00";
    annotation1.coordinate = CLLocationCoordinate2DMake(50.255625, 28.658819);
    [self.map addAnnotation:annotation1];
#pragma mark - Macdonalds
    FoodPlase *annotation2 = [FoodPlase new];
    annotation2.title = @"MacDonalds";
    annotation2.subtitle = @"Цілодобово";
    annotation2.coordinate = CLLocationCoordinate2DMake(50.2659, 28.6835);
    [self.map addAnnotation:annotation2];
 #pragma mark - Час поїсти
    FoodPlase *annotation3 = [FoodPlase new];
    annotation3.title = @"Час поїсти";
    annotation3.subtitle = @"(097)33-22-34 09:00-21:00";
    annotation3.coordinate = CLLocationCoordinate2DMake(50.2566, 28.6591);
    [self.map addAnnotation:annotation3];
 #pragma mark - +Cava
    FoodPlase *annotation4 = [FoodPlase new];
    annotation4.title = @"+Cava";
    annotation4.subtitle = @"(097)33-22-34 09:00-21:00";
    annotation4.coordinate = CLLocationCoordinate2DMake(50.2564, 28.6632);
    [self.map addAnnotation:annotation4];
#pragma mark - Entertiment
    entertainmentPlace *annotation12 = [entertainmentPlace new];
    annotation12.title = @"My Home";
    annotation12.subtitle = @"(097)33-22-34 09:00-21:00";
    annotation12.coordinate = CLLocationCoordinate2DMake(50.2575, 28.6750);
    [self.map addAnnotation:annotation12];
#pragma mark - Entertiment
    entertainmentPlace *annotation20 = [entertainmentPlace new];
    annotation20.title = @"Глобал";
    annotation20.subtitle = @"(097)33-22-34 09:00-21:00";
    annotation20.coordinate = CLLocationCoordinate2DMake(50.2662, 28.6860);
    [self.map addAnnotation:annotation20];
#pragma mark - Entertiment
    entertainmentPlace *annotation25 = [entertainmentPlace new];
    annotation25.title = @"Музей космонавтики";
    annotation25.subtitle = @"(097)33-22-34 09:00-21:00";
    annotation25.coordinate = CLLocationCoordinate2DMake(50.2536, 28.6780);
    [self.map addAnnotation:annotation25];
#pragma mark - Entertiment
    entertainmentPlace *annotation24 = [entertainmentPlace new];
    annotation24.title = @"Технологічний університет";
    annotation24.subtitle = @"(097)33-22-34 09:00-21:00";
    annotation24.coordinate = CLLocationCoordinate2DMake(50.2443, 28.6373);
    [self.map addAnnotation:annotation24];
#pragma mark - Entertiment
    entertainmentPlace *annotation21 = [entertainmentPlace new];
    annotation21.title = @"Кінотеатр Україна";
    annotation21.subtitle = @"(097)33-22-34 09:00-21:00";
    annotation21.coordinate = CLLocationCoordinate2DMake(50.2569, 28.6621);
    [self.map addAnnotation:annotation21];
#pragma mark - Entertiment
    entertainmentPlace *annotation22 = [entertainmentPlace new];
    annotation22.title = @"Автовокзал";
    annotation22.subtitle = @"(097)33-22-34 09:00-21:00";
    annotation22.coordinate = CLLocationCoordinate2DMake(50.2687, 28.6922);
    [self.map addAnnotation:annotation22];
#pragma mark - Entertiment
    entertainmentPlace *annotation23 = [entertainmentPlace new];
    annotation23.title = @"Парк";
    annotation23.subtitle = @"(097)33-22-34 09:00-21:00";
    annotation23.coordinate = CLLocationCoordinate2DMake(50.2514, 28.6698);
    [self.map addAnnotation:annotation23];
#pragma mark - Mario
    FoodPlase *annotation5 = [FoodPlase new];
    annotation5.title = @"Mario";
    annotation5.subtitle = @"(097)33-22-34 09:00-21:00";
    annotation5.coordinate = CLLocationCoordinate2DMake(50.2527, 28.6703);
    [self.map addAnnotation:annotation5];
#pragma mark - Pizza
    FoodPlase *annotation6 = [FoodPlase new];
    annotation6.title = @"Pizza";
    annotation6.subtitle = @"(097)33-22-34 08:00-21:00";
    annotation6.coordinate = CLLocationCoordinate2DMake(50.238674f, 28.695776f);
    [self.map addAnnotation:annotation6];
#pragma mark - Mac Chicken
    FoodPlase *annotation7 = [FoodPlase new];
    annotation7.title = @"Mac Chicken";
    annotation7.subtitle = @"(097)33-22-34 08:00-21:00";
    annotation7.coordinate = CLLocationCoordinate2DMake(50.258674f, 28.695776f);
    [self.map addAnnotation:annotation7];
#pragma mark - Mac Chicken
    FoodPlase *annotation8 = [FoodPlase new];
    annotation8.title = @"Чако паб";
    annotation8.subtitle = @"(097)33-22-34 08:00-21:00";
    annotation8.coordinate = CLLocationCoordinate2DMake(50.2578, 28.6692);
    [self.map addAnnotation:annotation8];
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    self.navigationController.navigationBarHidden = YES;
}

-(void)mapView:(MKMapView *)mapView didSelectAnnotationView:(MKAnnotationView *)view{
    self.titleAnn = view.annotation.title;
    self.Coordinate = view.annotation.coordinate;

}

-(MKAnnotationView *) mapView:(MKMapView *)mapView viewForAnnotation:(id<MKAnnotation>)annotation{

    if ([annotation isKindOfClass:[FoodPlase class]]) {
        FoodPlase *myLocation = (FoodPlase *)annotation;

        MKAnnotationView *annotationViewFood = [mapView dequeueReusableAnnotationViewWithIdentifier:@"foodPlase"];

        
        if (annotationViewFood == nil)
            annotationViewFood = myLocation.annotationViewFood;
        
        else
            annotationViewFood.annotation = annotation;
        UIButton* directionButton = [UIButton buttonWithType:UIButtonTypeContactAdd];
        [directionButton addTarget:self action:@selector(actionDirection:) forControlEvents:UIControlEventTouchUpInside];
        annotationViewFood.leftCalloutAccessoryView = directionButton;

        return annotationViewFood;
}

        if ([annotation isKindOfClass:[entertainmentPlace class]]) {
        entertainmentPlace *myLocation = (entertainmentPlace *)annotation;
        MKAnnotationView *annotationViewEntertainment = [mapView dequeueReusableAnnotationViewWithIdentifier:@"entertainmentPlace"];
        
        
        if (annotationViewEntertainment == nil)
            annotationViewEntertainment = myLocation.annotationViewEntertainment;
        
        else
            annotationViewEntertainment.annotation = annotation;
        
        UIButton* directionButton = [UIButton buttonWithType:UIButtonTypeContactAdd];
        [directionButton addTarget:self action:@selector(actionDirection:) forControlEvents:UIControlEventTouchUpInside];
        annotationViewEntertainment.leftCalloutAccessoryView = directionButton;
        
            return annotationViewEntertainment;
    }
        return nil;
    
    
}

- (MKOverlayRenderer *)mapView:(MKMapView *)mapView rendererForOverlay:(id <MKOverlay>)overlay {
        if ([overlay isKindOfClass:[MKPolyline class]]) {
            
            MKPolylineRenderer* renderer = [[MKPolylineRenderer alloc] initWithOverlay:overlay];
            renderer.lineWidth = 8.f;
            renderer.strokeColor = [UIColor colorWithRed:0.f green:0.8f blue:1.f alpha:0.9f];
            return renderer;
        }

        return nil;
    }

- (void) showAlertWithTitle:(NSString*) title andMessage:(NSString*) message {
    [[[UIAlertView alloc]
      initWithTitle:title
      message:message
      delegate:nil
      cancelButtonTitle:@"OK"
      otherButtonTitles:nil] show];
    
}

- (void) actionDirection:(UIButton*) sender {
    MKMapCamera *newCamera = [MKMapCamera camera];
    [newCamera setCenterCoordinate:self.map.userLocation.location.coordinate];
    [newCamera setPitch:88];
    [newCamera setHeading:180];
    [newCamera setAltitude:10];
    [newCamera setHeading:self.map.userLocation.location.course];
    //[newCamera setHeading:self.map.userLocation.location.course];
    [self.map setCamera:newCamera animated:YES];
    
     self.labelFinish.text = self.titleAnn;
    
     myTimerSetter = [NSTimer scheduledTimerWithTimeInterval:3.0f
                                                     target:self
                                                   selector:@selector(SetGPSData)
                                                   userInfo:nil
                                                    repeats:YES];
    
    myTimerSetterTwo = [NSTimer scheduledTimerWithTimeInterval:0.2f
                                                     target:self
                                                   selector:@selector(SetGPSSpeed)
                                                   userInfo:nil
                                                    repeats:YES];
    
}
-(void) SetGPSSpeed{
    if (self.map.userLocation.location.speed <= 0) {
        
        self.labelSpeed.text = [NSString stringWithFormat:@"0.0"];
    }
    else {
        self.labelSpeed.text = [NSString stringWithFormat:@"%0.1f", self.map.userLocation.location.speed*3.6];
    }

}

-(void) SetGPSData{
    
    if ([self.directions isCalculating]) {
        [self.directions cancel];
    }
    
    CLLocationCoordinate2D coordinate = self.Coordinate;
    
    request = [[MKDirectionsRequest alloc] init];
    
    request.source = [MKMapItem mapItemForCurrentLocation];
    
    MKPlacemark* placemark = [[MKPlacemark alloc] initWithCoordinate:coordinate
                                                   addressDictionary:nil];
    MKMapItem* destination = [[MKMapItem alloc] initWithPlacemark:placemark];
    
    request.destination = destination;
    
    request.requestsAlternateRoutes = YES;
    
    self.directions = [[MKDirections alloc] initWithRequest:request];
    
    [self.directions calculateDirectionsWithCompletionHandler:^(MKDirectionsResponse *response, NSError *error) {
        
        if (error) {
            [self showAlertWithTitle:@"Error" andMessage:[error localizedDescription]];
            if ([myTimerSetter isValid]) {
                [myTimerSetter invalidate];
            }
            return;
        }
        if ([response.routes count] == 0) {
            [self showAlertWithTitle:@"Error" andMessage:@"No routes found"];
            return;
        }
        
        [self.map removeOverlays:[self.map overlays]];
        
        NSMutableArray* array = [NSMutableArray array];
        
        
        for (MKRoute* route in response.routes) {
            [array addObject:route.polyline];
            
            self.labelFuel.text = [NSString stringWithFormat:@"%0.1f", route.distance/10000*6];
            self.outputKM.text = [NSString stringWithFormat:@"%0.1f", route.distance/1000];
            self.labelMany.text = [NSString stringWithFormat:@"%0.1f", route.distance/1000*3.0+10];
            self.labelTime.text = [NSString stringWithFormat:@"%0.1f", route.expectedTravelTime/60];
        }

        [self.map addOverlays:array level:MKOverlayLevelAboveRoads];
        
    }];
}

- (BOOL) shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)InterfaceOrientation {

    return (InterfaceOrientation != UIInterfaceOrientationPortraitUpsideDown);

}

#pragma mark - mapView
- (void)mapView:(MKMapView *)mapView didFailLoadWithError:(NSError *)error {
    UIAlertView* alert = [[UIAlertView alloc] initWithTitle:@"Помилка! Немає доступу до мережі  " message:[error localizedDescription] delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
    [alert show];
}
- (IBAction)moreInformationButtonPressed:(UIButton *)sender{
    MKCoordinateRegion region = MKCoordinateRegionMakeWithDistance(self.map.userLocation.location.coordinate, 1000.0, 1000.0);
    region.center = self.map.userLocation.location.coordinate;
    [self.map setRegion:region animated:YES];
}

- (IBAction)StopTtravelButton:(id)sender {
    if ([myTimerSetter isValid]) {
        [myTimerSetter invalidate];
    }
    self.labelSpeed.text = [NSString stringWithFormat:@"0.0"];
    self.outputKM.text = [NSString stringWithFormat: @"0.0"];
    self.labelMany.text = [NSString stringWithFormat:@"0.0"];
    self.labelTime.text = [NSString stringWithFormat:@"0.0"];
    self.labelFuel.text = [NSString stringWithFormat:@"0.0"];
}

#pragma mark - pressMashtab
- (IBAction)pressMashtab:(id)sender {
    MKCoordinateRegion region = MKCoordinateRegionMakeWithDistance(self.map.userLocation.location.coordinate, 5000.0, 5000.0);
    region.center = self.map.centerCoordinate;
    [self.map setRegion:region animated:YES];
    
}

- (IBAction)segmentedControlAction:(id)sender {
    
    switch  ((( UISegmentedControl  *) sender ). selectedSegmentIndex ) {
        
    case 0:
        self.map.mapType = MKMapTypeStandard;
        break;
    case 1:
        self.map.mapType = MKMapTypeSatellite;
        break;
    case 2:
        self.map.mapType = MKMapTypeHybrid;
        break;
        
    default:
        break;
}
}

- (IBAction)shareToFacebook:(id)sender  {

    if([SLComposeViewController isAvailableForServiceType:SLServiceTypeFacebook]){
        SLComposeViewController *facebookSheet = [SLComposeViewController
                                                  composeViewControllerForServiceType:SLServiceTypeFacebook];

        NSString *secondString = [self.titleAnn stringByAppendingString: @" буде нашим місцем збору, чекаю на всіх!"];
    
        [facebookSheet setInitialText:secondString];
        [facebookSheet addImage:[UIImage imageNamed:@"group.png"]];

        [self presentViewController:facebookSheet animated:YES completion:nil];
    }
    
}

#pragma mark - prepareForSegue

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    if([segue.identifier isEqualToString:@"toDetails"])
    {
        //detalViewController *detailVC = (detalViewController *)[segue destinationViewController];
        
    }}
@end

