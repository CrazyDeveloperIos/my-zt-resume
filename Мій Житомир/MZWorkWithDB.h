//
//  MZWorkWithDB.h
//  Мій Житомир
//
//  Created by Nazar Gorobets on 2/21/15.
//  Copyright (c) 2015 Nazar Gorobets. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>
#import "TaxiNamber.h"

@interface MZWorkWithDB : NSObject
-(NSArray *) getAllTaxi;
-(TaxiNamber *) getTaxiWithName:(NSString *)name;
-(void) save;
-(void)setupCoreData;
-(void)addNewTaxiWithName:(NSString *)name andNumber:(NSInteger)number;
-(void)removeTaxiWithObject:(TaxiNamber *) taxi;
+(MZWorkWithDB *) date;
@property (nonatomic) NSManagedObjectContext *managedObjectContext;
@end
