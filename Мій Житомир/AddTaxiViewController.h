//
//  AddTaxiViewController.h
//  Мій Житомир
//
//  Created by Nazar Gorobets on 2/27/15.
//  Copyright (c) 2015 Nazar Gorobets. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TaxiTableViewController.h"

@interface AddTaxiViewController : UIViewController
- (IBAction)saveButton:(id)sender;
@property (weak, nonatomic) IBOutlet UITextField *nameFild;
@property (weak, nonatomic) IBOutlet UITextField *numberFild;

@end
