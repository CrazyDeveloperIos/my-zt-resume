//
//  FoodPlase.m
//  Мій Житомир
//
//  Created by Nazar Gorobets on 3/12/15.
//  Copyright (c) 2015 Nazar Gorobets. All rights reserved.
//

#import "FoodPlase.h"
#import <MapKit/MapKit.h>

@implementation FoodPlase
@synthesize coordinate;
@synthesize title;
@synthesize subtitle;
//тест

-(MKAnnotationView *)annotationViewFood{
    
    MKAnnotationView *annotationViewFood = [[MKAnnotationView alloc] initWithAnnotation:self reuseIdentifier:@"FoodPlase"];
    
    annotationViewFood.enabled=YES;
    annotationViewFood.canShowCallout = YES;
    annotationViewFood.image = [UIImage imageNamed:@"pinsFoods"];
    annotationViewFood.leftCalloutAccessoryView = [UIButton buttonWithType:UIButtonTypeInfoLight];
    annotationViewFood.canShowCallout = YES;
    
    UIImageView *sfIconView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"Ico1"]];
    annotationViewFood.rightCalloutAccessoryView = sfIconView;
    return annotationViewFood;
}

@end