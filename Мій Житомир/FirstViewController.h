//
//  FirstViewController.h
//  Мій Житомир
//
//  Created by Yosemite on 14.11.14.
//  Copyright (c) 2014 Nazar Gorobets. All rights reserved.
//
#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import <MapKit/MapKit.h>


@interface FirstViewController : UIViewController <MKMapViewDelegate>{
    
    __weak IBOutlet UISegmentedControl *segmentedControl;
    
}
@property (nonatomic, strong) NSString* titleAnn;
@property (weak, nonatomic) IBOutlet MKMapView *map;
- (IBAction)moreInformationButtonPressed:(UIButton *)sender;
- (IBAction)StopTtravelButton:(id)sender;
- (IBAction)pressMashtab:(UIButton *)sender;
- (IBAction)segmentedControlAction:(id)sender;
- (IBAction)shareToFacebook:(id)sender;
@property (weak, nonatomic) IBOutlet UILabel *outputKM;
@property (weak, nonatomic) IBOutlet UILabel *labelMany;
@property (weak, nonatomic) IBOutlet UILabel *labelTime;
@property (weak, nonatomic) IBOutlet UILabel *labelSpeed;
@property (weak, nonatomic) IBOutlet UILabel *labelFuel;
@property (weak, nonatomic) IBOutlet UILabel *labelFinish;
@property (nonatomic, retain) NSTimer *myTimerSetter;
@property (nonatomic, retain) NSTimer *myTimerSetterTwo;

@end