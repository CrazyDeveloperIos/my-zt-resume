//
//  foodPlace.h
//  Мій Житомир
//
//  Created by Nazar Gorobets on 22.12.14.
//  Copyright (c) 2014 Nazar Gorobets. All rights reserved.
//

#import <MapKit/MapKit.h>
#import <Foundation/Foundation.h>

@interface foodPlace : NSObject <MKAnnotation>

@property (nonatomic, readonly) CLLocationCoordinate2D cordinate;
@property (copy, nonatomic) NSString *title;

-(id) initWithTitle:(NSString*) newTitle Location:(CLLocationCoordinate2D)location;

-(MKAnnotationView *)annotationView;

@end
