//
//  еntertainmentPlace.h
//  Мій Житомир
//
//  Created by Nazar Gorobets on 23.12.14.
//  Copyright (c) 2014 Nazar Gorobets. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface entertainmentPlace : NSObject<MKAnnotation>

@property (nonatomic, assign) CLLocationCoordinate2D coordinate;
@property (nonatomic, copy) NSString *title;
@property (nonatomic, copy) NSString *subtitle;

-(MKAnnotationView *)annotationViewEntertainment;

@end
