//
//  TaxiNamber.h
//  Мій Житомир
//
//  Created by Nazar Gorobets on 2/21/15.
//  Copyright (c) 2015 Nazar Gorobets. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface TaxiNamber : NSManagedObject

@property (nonatomic, retain) NSString * nameTaxi;
@property (nonatomic, retain) NSNumber * phoneNamber;

@end
