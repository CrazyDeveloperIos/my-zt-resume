//
//  myAnnotation.m
//  Мій Житомир
//
//  Created by Nazar Gorobets on 13.12.14.
//  Copyright (c) 2014 Nazar Gorobets. All rights reserved.
//

#import "myAnnotation.h"

@implementation myAnnotation

@synthesize coordinate = _coordinate;


-(id)initWithTitle:(NSString *)newTitle Location:(CLLocationCoordinate2D)location{
    
    self =  [super init];
    _title = newTitle;
    _coordinate = location;
    return self;
}

-(MKAnnotationView *)annotationView{
    
    MKAnnotationView *annotationView = [[MKAnnotationView alloc] initWithAnnotation:self reuseIdentifier:@"myAnnotation"];
    
    annotationView.enabled=YES;
    annotationView.canShowCallout = YES;
    annotationView.image = [UIImage imageNamed:@"fork7"];
    annotationView.rightCalloutAccessoryView = [UIButton buttonWithType:UIButtonTypeDetailDisclosure];
    
    
    CLLocationCoordinate2D myCoordinate = {51.255625, 29.658819};
    
    MKPointAnnotation *point = [[MKPointAnnotation alloc] init];
    
    point.coordinate = myCoordinate;

    return annotationView;
    
}

@end
