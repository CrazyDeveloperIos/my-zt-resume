//
//  AddTaxiViewController.m
//  Мій Житомир
//
//  Created by Nazar Gorobets on 2/27/15.
//  Copyright (c) 2015 Nazar Gorobets. All rights reserved.
//

#import "AddTaxiViewController.h"
#import "MZWorkWithDB.h"

@interface AddTaxiViewController ()

@end

@implementation AddTaxiViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



- (IBAction)saveButton:(id)sender {
    UIAlertView *alert = nil;
    if ([self.nameFild.text length] && [self.numberFild.text length]){
        [[MZWorkWithDB date] addNewTaxiWithName:self.nameFild.text andNumber:[self.numberFild.text integerValue]];
        alert = [[UIAlertView alloc] initWithTitle:@"Ура!" message:@"Вдало додано!" delegate:self cancelButtonTitle:@"ok" otherButtonTitles: nil];
    }else{
        alert = [[UIAlertView alloc] initWithTitle:@"Ви не заповнили всі поля" message:@"Спробуйте ще раз!" delegate:self cancelButtonTitle:@"ok" otherButtonTitles: nil];
    }
        [alert show];
    
    
}
@end
