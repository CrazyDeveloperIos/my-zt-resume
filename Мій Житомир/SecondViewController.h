//
//  SecondViewController.h
//  Мій Житомир
//
//  Created by Yosemite on 14.11.14.
//  Copyright (c) 2014 Nazar Gorobets. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SecondViewController : UIViewController <UIWebViewDelegate>

@property (weak, nonatomic) IBOutlet UIWebView *veb_line;
@end
