//
//  MZWorkWithDB.m
//  Мій Житомир
//
//  Created by Nazar Gorobets on 2/21/15.
//  Copyright (c) 2015 Nazar Gorobets. All rights reserved.
//

#import "MZWorkWithDB.h"
static NSString *dataStoreName = @"BaseTaxi.sqlite";
@interface MZWorkWithDB()
@property (nonatomic, strong) NSPersistentStore * dataStore;
@property (nonatomic, strong) NSManagedObjectModel         *model;
@property (nonatomic, strong) NSPersistentStoreCoordinator *coordinator;
@end

@implementation MZWorkWithDB
@synthesize dataStore;
+(MZWorkWithDB *)date{
    static MZWorkWithDB* dataBaseCon;
    if(!dataBaseCon){
        dataBaseCon = [[MZWorkWithDB alloc] init];
    }
    return dataBaseCon;
}
-(instancetype)init{
    self = [super init];
    if (!self) {return nil;}
    
    self.model = [NSManagedObjectModel mergedModelFromBundles:nil];
    self.coordinator = [[NSPersistentStoreCoordinator alloc]
                    initWithManagedObjectModel:self.model];
    self.managedObjectContext = [[NSManagedObjectContext alloc]
                initWithConcurrencyType:NSMainQueueConcurrencyType];
    [self.managedObjectContext setPersistentStoreCoordinator:self.coordinator];
    return self;
}
-(NSPersistentStore *)dataStore{
    if (!dataStore){
         NSError *error = nil;
        dataStore = [self.coordinator addPersistentStoreWithType:NSSQLiteStoreType
                                            configuration:nil
                                                      URL:[self storeURL]
                                                  options:nil error:&error];
    }
    
    return dataStore;
}
- (NSString *)applicationDocumentsDirectory {

    return [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask,YES) lastObject];
}
- (NSURL *)applicationStoresDirectory {
    NSURL *storesDirectory =
    [[NSURL fileURLWithPath:[self applicationDocumentsDirectory]]
     URLByAppendingPathComponent:@"Stores"];
    
    NSFileManager *fileManager = [NSFileManager defaultManager];
    if (![fileManager fileExistsAtPath:[storesDirectory path]]) {
        NSError *error = nil;
        if ([fileManager createDirectoryAtURL:storesDirectory
                  withIntermediateDirectories:YES
                                   attributes:nil
                                        error:&error]) {
        }
        else {NSLog(@"FAILED to create Stores directory: %@", error);}
    }
    return storesDirectory;
}

- (NSURL *)storeURL {
    return [[self applicationStoresDirectory]
            URLByAppendingPathComponent:dataStoreName];
}
-(void)setupCoreData{
    dataStore = [self.coordinator addPersistentStoreWithType:NSSQLiteStoreType
                                               configuration:nil
                                                         URL:[self storeURL]
                                                     options:nil error:nil];
}

-(void)save
{
    NSError *error = nil;
    if ([self.managedObjectContext save:&error]) {
        NSLog(@"_context SAVED changes to persistent store");
    } else {
        NSLog(@"Failed to save _context: %@", error);
    }
}
-(void)addNewTaxiWithName:(NSString *)name andNumber:(NSInteger)number{
    TaxiNamber *taxi = [NSEntityDescription
                        insertNewObjectForEntityForName:@"TaxiNamber"
                        inManagedObjectContext:self.managedObjectContext];;
    taxi.nameTaxi  = name;
    taxi.phoneNamber  = [NSNumber numberWithInteger:number];
    [self.managedObjectContext insertObject:taxi];

    [self  save];
}

-(void)removeTaxiWithObject:(TaxiNamber *) taxi{
    [self.managedObjectContext deleteObject:taxi];
    [self  save];
}

-(NSArray *)getAllTaxi{
    NSManagedObjectContext *moc = [self managedObjectContext];
    NSEntityDescription *entityDescription = [NSEntityDescription
                                              entityForName:@"TaxiNamber" inManagedObjectContext:moc];
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    [request setEntity:entityDescription];
    
    // Set example predicate and sort orderings...
        [request setPredicate:nil];
    
    NSError *error;
    NSArray *array = [moc executeFetchRequest:request error:&error];
    return array;
}
-(TaxiNamber *)getTaxiWithName:(NSString *)name{
    NSManagedObjectContext *moc = [self managedObjectContext];
    NSEntityDescription *entityDescription = [NSEntityDescription
                                              entityForName:@"TaxiNamber" inManagedObjectContext:moc];
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    [request setEntity:entityDescription];
    
    
    NSPredicate *preficate = [NSPredicate predicateWithFormat:@"nameTaxi like %@", name];
    [request setPredicate:preficate];
    // Set example predicate and sort orderings...
    
    NSError *error;
    NSArray *array = [moc executeFetchRequest:request error:&error];
    return [array firstObject];
}
@end
