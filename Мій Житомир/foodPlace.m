//
//  foodPlace.m
//  Мій Житомир
//
//  Created by Nazar Gorobets on 22.12.14.
//  Copyright (c) 2014 Nazar Gorobets. All rights reserved.
//

#import "foodPlace.h"

@implementation foodPlace

@synthesize coordinate = _coordinate;


-(id)initWithTitle:(NSString *)newTitle Location:(CLLocationCoordinate2D)location{
    
    self =  [super init];
    _title = newTitle;
    _coordinate = location;
    return self;
}

-(MKAnnotationView *)annotationView{
    
    MKAnnotationView *annotationView = [[MKAnnotationView alloc] initWithAnnotation:self reuseIdentifier:@"Тут можна поїсти"];
    
    annotationView.enabled=YES;
    annotationView.canShowCallout = YES;
    annotationView.image = [UIImage imageNamed:@"AnImg"];
    annotationView.rightCalloutAccessoryView = [UIButton buttonWithType:UIButtonTypeDetailDisclosure];
    
    
    CLLocationCoordinate2D myCoordinate = {50.255625, 29.658819};
    
    MKPointAnnotation *point = [[MKPointAnnotation alloc] init];
    
    point.coordinate = myCoordinate;

    return annotationView;
    
}

@end
