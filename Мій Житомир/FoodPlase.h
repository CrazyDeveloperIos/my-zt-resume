//
//  FoodPlase.h
//  Мій Житомир
//
//  Created by Nazar Gorobets on 3/12/15.
//  Copyright (c) 2015 Nazar Gorobets. All rights reserved.
//
#import <MapKit/MapKit.h>
#import <Foundation/Foundation.h>

@interface FoodPlase : NSObject<MKAnnotation>

@property (nonatomic, assign) CLLocationCoordinate2D coordinate;
@property (nonatomic, copy) NSString *title;
@property (nonatomic, copy) NSString *subtitle;

-(MKAnnotationView *)annotationViewFood;

@end
