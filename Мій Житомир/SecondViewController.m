//
//  SecondViewController.m
//  Мій Житомир
//
//  Created by Yosemite on 14.11.14.
//  Copyright (c) 2014 Nazar Gorobets. All rights reserved.
//

#import "SecondViewController.h"

@interface SecondViewController ()
@property (weak, nonatomic) IBOutlet UIToolbar *toolBar;

@end

@implementation SecondViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
      NSURL* url = [NSURL URLWithString:@"http://www.zhitomir.info/"];
    NSURLRequest * request = [NSURLRequest requestWithURL:url];
    [_veb_line loadRequest:request];
//    [_veb_line setScalesPageToFit:YES];
    [_veb_line setDelegate:self];
    [self.view bringSubviewToFront:self.toolBar];
    
}


- (void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error {
    UIAlertView* alert = [[UIAlertView alloc] initWithTitle:@"Помилка! Немає доступу до мережі" message:[error localizedDescription] delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
    [alert show];
}

- (void)webViewDidStartLoad:(UIWebView *)webView {
    [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:YES];
}
- (void)webViewDidFinishLoad:(UIWebView *)webView {
    [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
