//
//  AddingTaxiTableViewController.h
//  Мій Житомир
//
//  Created by Nazar Gorobets on 2/17/15.
//  Copyright (c) 2015 Nazar Gorobets. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MZWorkWithDB.h"

@interface AddingTaxiTableViewController : UIViewController
@property (weak, nonatomic) IBOutlet UITextField *nameTaxiFild;
@property (weak, nonatomic) IBOutlet UITextField *namberPhoneFild;
- (IBAction)saveContext:(id)sender;
@end
